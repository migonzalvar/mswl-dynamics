/* Descriptive information about repositories in a database */

USE cvsanaly_android_2;

SELECT name, count(*), min(date), max(date)
from scmlog left join repositories on (scmlog.repository_id = repositories.id)
group by 1
order by 2 desc;


/* Generations: slots_10 */
SELECT count(*), min(date), max(date) FROM scmlog;

SELECT
	(TO_DAYS(s.date) - TO_DAYS("2008-10-21 07:00:00")) DIV
	((TO_DAYS("2012-04-04 14:18:27") - TO_DAYS("2008-10-21 07:00:00")) DIV 10) period,
	s.author_id committer,
	COUNT(DISTINCT(s.id)) ncommits
	FROM scmlog s, actions a, file_types ft
	WHERE a.commit_id = s.id
	AND a.file_id = ft.file_id
	AND ft.type IN ('unknown', 'code')
	GROUP BY period, committer
	ORDER BY period, ncommits DESC;

