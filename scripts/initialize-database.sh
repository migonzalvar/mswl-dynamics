#!/usr/bin/bash

mysqladmin -u root -p create cvsanaly_android_2

echo "CREATE USER 'cvsanaly'@'localhost' IDENTIFIED BY 'cvsanaly';" |\
	mysql -u root -p

echo "GRANT ALL ON cvsanaly_android_2.* TO 'cvsanaly'@'localhost';" |\
	mysql -u root -p
