#!/bin/bash
source ~/envs/dynamics/bin/activate

./drop_database.sh
./generations_base.sh

./drop_database.sh
./generations_system.sh

./drop_database.sh
./generations_apps.sh

./drop_database.sh
./generations_dalvik.sh

./drop_database.sh
./generations_sdk.sh

./generations.R BASE
./generations.R SYSTEM
./generations.R APPS
./generations.R DALVIK
./generations.R SDK

