#!/usr/bin/env python
#
#  Copyright 2012 Miguel Gonzalez <migonzalvar@gmail.com>
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

import argparse
import string
import subprocess
import sys

# To create the database:
#
# $ mysqladmin -u root create cvsanaly_android
# > CREATE USER 'cvsanaly'@'localhost' IDENTIFIED BY 'cvsanaly';
# > GRANT ALL ON cvsanaly_android.* TO 'cvsanaly'@'localhost';


def debug(msg):
    print "DEBUG:", msg


def cvsanaly(cvsanaly_conf, directory):
    my_command = string.Template(
           "cvsanaly2"
           " --db-driver=$driver"
           " --db-user=$user"
           " --db-password=$password"
           " --db-database=$database"
           " --extensions=$extensions"
           " $directory")
    my_command = my_command.substitute(cvsanaly_conf, directory=directory)
    debug(my_command)
    subprocess.call(my_command, shell=True)


def drop_generations_database(generations_conf):
    my_command = string.Template(
            "echo "
            "\"DROP DATABASE IF EXISTS $output_database;"
            "  CREATE DATABASE $output_database;\" |"
            "  mysql -u root -proot ")
    my_command = my_command.substitute(generations_conf)
    debug(my_command)
    subprocess.call(my_command, shell=True)


def generations(generations_conf, repository):
    my_command = string.Template(
            "generations.py"
            " -u $user"
            " -p $password"
            " -d $database"
            " --db-driver $driver"
            " --db-output-user $output_user"
            " --db-output-password $output_password"
            " --db-output-database $output_database"
            " --output-dir $output_dir/$repository"
            " --repository $repository")
    my_command = my_command.substitute(generations_conf,
            repository=repository)
    debug(my_command)
    subprocess.call(my_command, shell=True)


def read_folder_list(filename):
    f = open(filename, 'r')
    folder_list = []
    for uri in f.readlines():
        name = uri.rstrip("/").split("/")[-1].strip()
        folder_list.append(name)
    f.close()
    return folder_list

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('command', choices=['cvsanaly', 'generations'])
    parser.add_argument('folders', metavar='repo', action='store', type=str, nargs='+',
                        help='the repository to perform the command')

    args = parser.parse_args()


    cvsanaly_conf = dict(driver="mysql", user="cvsanaly",
            password="cvsanaly", database="cvsanaly_android",
            extensions="FileTypes")

    if args.command == 'cvsanaly':
        for n, folder in enumerate(args.folders, start=1):
            print "Importing repo %s (%s/%s)" % (folder, n, len(args.folders))
            cvsanaly(cvsanaly_conf, folder)

    generations_conf = cvsanaly_conf
    generations_conf.update(output_user="root")
    generations_conf.update(output_password="root")
    generations_conf.update(output_database="android_gens")
    generations_conf.update(output_dir="output")

    if args.command == 'generations':
        for n, folder in enumerate(args.folders, start=1):
            print "Generations of repo %s (%s/%s)" % (folder, n, len(args.folders))
            drop_generations_database(generations_conf)
            generations(generations_conf, repository=folder+".git")

